<?php
	 
	/*
	 * Following code will get single post's details
	 * A post is identified by post id (pid)
	 */
	 
	// array for JSON response
	$response = array();
	
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	 
	// connecting to db
	$db = new DB_CONNECT();
	 
	// check for post data
	if (isset($_GET["pid"])) {
	    $pid = mysql_real_escape_string($_GET["pid"]);
	 
	    // get a post from posts table
	    $result = mysql_query("SELECT *FROM posts WHERE pid = $pid");
	 
	    if (!empty($result)) {
	        // check for empty result
	        if (mysql_num_rows($result) > 0) {
	 
	            $result = mysql_fetch_array($result);
	 
	            $post= array();
	            $post["pid"] = $result["pid"];
	            $post["name"] = $result["name"];
	            $post["title"] = $result["title"];
	            $post["body"] = $result["body"];
	            $post["upvotes"] = $result["upvotes"];
	            $post["downvotes"] = $result["downvotes"];
	            $post["created_at"] = $result["created_at"];
	            $post["updated_at"] = $result["updated_at"];
		    $post["sighting_time"] = $result['sighting_time'];
		    $post["sighting_date"] = $result['sighting_date'];
		    $post["latlong"] = $result['latlong'];
	            
	            $image_urls = array();
	            $image_records = mysql_query("SELECT * FROM images WHERE pid = $pid");
	            
	            while($row = mysql_fetch_assoc($image_records))
	            {
	            	array_push($image_urls, "http://crazychimps.com/climatewatch/" . $row["path"]);
	            }
	            
	            // success
	            $response["success"] = 1;
	 
	            // user node
	            $response["post"] = array();
	            $response["images"] = array();
	 
	            array_push($response["post"], $post);
	            $response["images"] = $image_urls;
	 
	            // echoing JSON response
	            echo json_encode($response);
	        } else {
	            // no post found
	            $response["success"] = 0;
	            $response["message"] = "No post found";
	 
	            // echo no users JSON
	            echo json_encode($response);
	        }
	    } else {
	        // no post found
	        $response["success"] = 0;
	        $response["message"] = "No post found";
	 
	        // echo no users JSON
	        echo json_encode($response);
	    }
	} else {
	    // required field is missing
	    $response["success"] = 0;
	    $response["message"] = "Required field(s) is missing";
	 
	    // echoing JSON response
	    echo json_encode($response);
	}
?>