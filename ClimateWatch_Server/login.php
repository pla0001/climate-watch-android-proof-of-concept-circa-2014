<?php
	 
	/*
	 * Following code will log in the user
	 * All details are read from HTTP Post Request
	 */
		
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	
	// connecting to db
	$db = new DB_CONNECT();
	 
	// array for JSON response
	$response = array();
	 
	// check for required fields
	if (isset($_POST['username']) && isset($_POST['password'])) {
	 
		$username = mysql_real_escape_string($_POST['username']);
		$password = mysql_real_escape_string($_POST['password']);
		
		$result = array();
		
		$result = mysql_query("SELECT * FROM accounts WHERE username = '$username' AND password = '$password'");
		$details = mysql_fetch_array($result);
	 	if(mysql_num_rows($result) > 0){
	 		$response["name"] = $details["name"];
	 		$response["username"] = $details["username"];
	 		$response["password"] = $details["password"];
	 		$response["email"] = $details["email"];
	 		$response["created_at"] = $details["created_at"];
	 		$response["profile_image_url"] = $details["profile_image_url"];
			$response["success"] = 1;
			$response["message"] = "Logged in as " . $username;
		 
		        // echoing JSON response
		        echo json_encode($response);
		}else{
			$response["success"] = 0;
			$response["message"] = "Username or password is incorrect.";
			echo json_encode($response);
		}
	        
	} else {
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is missing";
		
		// echoing JSON response
		echo json_encode($response);
	}
?>