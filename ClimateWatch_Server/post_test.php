<?php
	 
	/*
	 * Following code will create a new product row
	 * All product details are read from HTTP Post Request
	 */
		
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	
	// connecting to db
	$db = new DB_CONNECT();
	 
	// array for JSON response
	$response = array();
	 
	// check for required fields
	if (isset($_POST['name']) && isset($_POST['title']) && isset($_POST['body'])) {
	 
		$name = mysql_real_escape_string($_POST['name']);
		$title = mysql_real_escape_string($_POST['title']);
		$body = mysql_real_escape_string($_POST['body']);
		$created_at = mysql_real_escape_string($_POST['created_at']);
		$sighting_time = mysql_real_escape_string($_POST['sighting_time']);
		$sighting_date = mysql_real_escape_string($_POST['sighting_date']);
		$latlong = mysql_real_escape_string($_POST['latlong']);
		
		// mysql inserting a new row
		$result = mysql_query("INSERT INTO posts(name, title, body, created_at, latlong, sighting_time, sighting_date) VALUES('$name', '$title', '$body', '$created_at', '$latlong', '$sighting_time', '$sighting_date')");
		 
		 $record = mysql_query("SELECT * FROM posts WHERE name = '$name' AND title = '$title'");
		 $pid = mysql_fetch_array($record)['pid'];
 
	 	if($result){
			$response["success"] = 1;
			$response["message"] = "post successfully created.";
			$response["pid"] = $pid;
		 
		        // echoing JSON response
		        echo json_encode($response);
		} else {
		        // failed to insert row
		        $response["success"] = 0;
		        $response["message"] = "Oops! An error occurred.";
		 
		        // echoing JSON response
		        echo json_encode($response);
	        }
	        
	} else {
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is missing";
		
		// echoing JSON response
		echo json_encode($response);
	}
?>