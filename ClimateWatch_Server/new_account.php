<?php
	 
	/*
	 * Following code will create a new account
	 * All account details are read from HTTP Post Request
	 */
		
	// include db connect class
	require_once __DIR__ . '/db_connect.php';
	
	// connecting to db
	$db = new DB_CONNECT();
	 
	// array for JSON response
	$response = array();
	 
	// check for required fields
	if (isset($_POST['name']) && isset($_POST['username']) && isset($_POST['password']) && isset($_POST['email'])) {
	 
	 	$name = mysql_real_escape_string($_POST['name']);
		$username = mysql_real_escape_string($_POST['username']);
		$password = mysql_real_escape_string($_POST['password']);
		$email = mysql_real_escape_string($_POST['email']);
		$created_at = mysql_real_escape_string($_POST['created_at']);
		
		$result = array();
		
		$result = mysql_query("SELECT * FROM accounts WHERE username = '$username'");
		if(empty($result)){
			$response["success"] = 0;
			$response["message"] = "Username already in use.";
			echo json_encode($response);
		}else{
			
			$result = mysql_query("INSERT INTO accounts(name, username, password, email, created_at) VALUES('$name', '$username', '$password', '$email', '$created_at')");
	 
		 	if($result){
				$response["success"] = 1;
				$response["message"] = "Account created successfully.";
			 
			        // echoing JSON response
			        echo json_encode($response);
			} else {
			        // failed to insert row
			        $response["success"] = 0;
			        $response["message"] = "Oops! An error occurred.";
			 
			        // echoing JSON response
			        echo json_encode($response);
		        }
		}
	        
	} else {
		// required field is missing
		$response["success"] = 0;
		$response["message"] = "Required field(s) is missing";
		
		// echoing JSON response
		echo json_encode($response);
	}
?>