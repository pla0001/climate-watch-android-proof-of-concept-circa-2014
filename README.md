# ClimateWatch (Android)
An android app that uses reddit technology to create a powerful social platform for people to engage in citizen science and help understand more of climate change.

## Developers Involved

* Itay Plavin
* Konrad Cybulski
* Saurabh Joshi
* Rohan Malik

## Android Targets
Minimum SDK required: 8, Android 2.2, Froyo

Target SDK: 20, Android L/4.4W

Compiles with: Kitkat

## Useful Links
[LucidChart (UI Design)](https://www.lucidchart.com/documents/edit/26d2525a-bb5f-444b-8d43-b669a2663377)

[Trello Board](https://trello.com/b/CU9gKHl1/reddit-feature)

[Computational Science Project Assignment Instructions and Rubric](https://docs.google.com/a/jmss.vic.edu.au/document/d/1l7VtMlImpWsKVLCuw15KwPK4nHBXumPh4CbYJB20Eh0)

[Group's Google Document for Computational Science Project](https://developer.android.com/tools/support-library/setup.html)

[Official ClimateWatch Website](http://www.climatewatch.org.au/)

## Setup

You will need Eclipse with an Android SDK to be able to edit this code. Installing EGit is also handy since it'll allow direct version control management, without having to swap between a terminal and Eclipse in order to use git. 

Once installed, go to Window -> Show View -> Other..., then expand the Git folder and select "Git Repositories". In the "Git Repositories" view, select clone existing repository, then paste the https link in provided in the top right corner of the bitbucket repo. Provide your password, then click clone to clone the repository.

Right click on climate-watch-android, then select 'Import Projects...'. Make sure that 'Import existing projects' is checked, then click Next and hit Finish to import the ClimateWatch project.

Since this Android app depends on appcompat v7, you'll need to add the library in order for the app to build and work. Refer to [Support Library Setup](https://developer.android.com/tools/support-library/setup.html) on how to solve this issue. Follow the instructions under 'Adding libraries with resources' in 'Using Eclipse' section. 

## Quality Control

### Commit comments
All commits should begin with one of three words: "Added", "Changed", or "Removed". Keep it as concise as possible, and no more than three sentences. Order the sentence in the most important to the least important. Although appreciated, you do not have to specify every file when committing the repository, as git can already show exactly what files have been modified, and in particular which lines have been changed.  

### Formatting Source Code
Set Eclipse to automatically format java source code on save by going into Window -> Preferences -> Java -> Editor -> Save Actions, then check "Perform the selected actions on save". Make sure that "Format source code" and "Format all lines" are both checked. Hit OK to save and exit the preferences window. 

For formatting XML files on save in Eclipse, goto Window -> Preferences -> Android -> Editors, then check "Format on Save". Hit OK to save and exit the preferences window.
