package org.earthwatch.climatewatch;

import java.io.File;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class CreatePostActivity extends ActionBarActivity {
    // Progress Dialog
    private ProgressDialog pDialog;

    JSONParser jsonParser = new JSONParser();
    EditText inputName;
    EditText inputTitle;
    EditText inputBody;
    EditText inputQuantity;
    ArrayList<Bitmap> photos = new ArrayList<Bitmap>();
    ArrayList<String> photoPaths = new ArrayList<String>();
    LinearLayout photoScroll;
    ImageButton imageAddBtn;
    
    // Last modified time
    
    private long lastModified = 0;

    // url to create new product
    private static String url_create_product = "http://crazychimps.com/climatewatch/post_test.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    JSONObject json;

    String message = "";
    int success = 0;
    /*
     * If there is an error from the client side, this variable should be set
     * one of the form error constants, then treated appropriately after the
     * task has been performed.
     */
    int error_type = 0;

    @SuppressLint("NewApi")
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.add_post);
	getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	// Edit Text
	// inputName = (EditText) findViewById(R.id.inputName);
	inputTitle = (EditText) findViewById(R.id.inputTitle);
	inputBody = (EditText) findViewById(R.id.inputBody);
	inputQuantity = (EditText) findViewById(R.id.inputQuantity);

	// Create button
	Button btnCreateProduct = (Button) findViewById(R.id.btnCreateProduct);
	// photoScroll = (LinearLayout)findViewById(R.id.photoScrollLayout);
	// imageAddBtn = (ImageButton)findViewById(R.id.photoImageButton);

	// button click event
	btnCreateProduct.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View view) {
		// creating new product in background thread
		new CreateNewProduct().execute();
	    }
	});

	// Edit Text
	// inputName = (EditText) findViewById(R.id.inputName);
	inputTitle = (EditText) findViewById(R.id.inputTitle);
	inputBody = (EditText) findViewById(R.id.inputBody);

	// Create button
	btnCreateProduct = (Button) findViewById(R.id.btnCreateProduct);
	photoScroll = (LinearLayout) findViewById(R.id.photoScrollLayout);
	imageAddBtn = (ImageButton) findViewById(R.id.photoImageButton);

	// button click event
	btnCreateProduct.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View view) {
		// creating new product in background thread
		new CreateNewProduct().execute();
	    }
	});
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == android.R.id.home) {
	    confirmExitCreatePost();
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
	confirmExitCreatePost();
    }

    public void confirmExitCreatePost() {
	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
		CreatePostActivity.this);
	AlertDialog alertDialog = alertDialogBuilder.create();

	alertDialog.setTitle("Leave create post?");
	alertDialog.setMessage("Are you sure you want to leave this new post?");

	final int BUTTON_POSITIVE = -1;
	final int BUTTON_NEGATIVE = -2;
	alertDialog.setButton(BUTTON_POSITIVE, "Yes",
		new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
			finish();
		    }
		});
	alertDialog.setButton(BUTTON_NEGATIVE, "No",
		new DialogInterface.OnClickListener() {
		    @Override
		    public void onClick(DialogInterface dialog, int which) {
			// do nothing
		    }
		});

	// Set the icon to warning icon
	alertDialog.setIcon(R.drawable.ic_warning);

	// Show the alert dialog to user
	alertDialog.show();
    }

    int IMAGE_PICKER_SELECT = 2557;

    public void onAddPhotoClick(View view) {
	Intent i = new Intent(Intent.ACTION_PICK,
		android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	startActivityForResult(i, IMAGE_PICKER_SELECT);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (requestCode == IMAGE_PICKER_SELECT
		&& resultCode == Activity.RESULT_OK) {
	    String bitmapPath = getPathFromCameraData(data, this);
	    photoPaths.add(bitmapPath);
	}
	runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		updatePhotoScroll();
	    }
	});
    }

    public String getPathFromCameraData(Intent data, Context context) {
	Uri selectedImage = data.getData();
	String[] filePathColumn = { MediaStore.Images.Media.DATA };
	Cursor cursor = context.getContentResolver().query(selectedImage,
		filePathColumn, null, null, null);
	cursor.moveToFirst();
	int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	String picturePath = cursor.getString(columnIndex);

	// Check when the image was last modified
	// This will be the time the user took the image
	// And hence the sighting date/time

	File file = new File(picturePath);
	lastModified = file.lastModified();



	// TODO: Add date and time to the json bundle that will be sent to the
	// server.

	return picturePath;
	// cursor.close();
	// return BitmapFactory.decodeFile(picturePath);
    }

    private void updatePhotoScroll() {
	photoScroll.removeAllViews();
	for (String b : photoPaths) {
	    ImageView picB = new ImageView(this);
	    Bitmap bitmap = BitmapFactory.decodeFile(b);

	    DisplayMetrics displaymetrics = new DisplayMetrics();
	    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
	    int height = displaymetrics.heightPixels;
	    int width = displaymetrics.widthPixels;

	    final int maxSize = (int) (width * 0.5);
	    int outWidth;
	    int outHeight;
	    int inWidth = bitmap.getWidth();
	    int inHeight = bitmap.getHeight();
	    if (inWidth > inHeight) {
		outWidth = maxSize;
		outHeight = (inHeight * maxSize) / inWidth;
	    } else {
		outHeight = maxSize;
		outWidth = (inWidth * maxSize) / inHeight;
	    }

	    Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth,
		    outHeight, false);

	    picB.setImageBitmap(resizedBitmap);
	    picB.setScaleType(ScaleType.FIT_CENTER);
	    LayoutParams lp = new LayoutParams((int) (outWidth * 1.1),
		    (int) (outHeight * 1.1));
	    photoScroll.addView(picB, lp);
	}
	if (photoPaths.size() > 0) {
	    imageAddBtn.setImageResource(R.drawable.cam_button_pressed);
	} else {
	    imageAddBtn.setImageResource(R.drawable.cam_button_unpressed);
	}
    }

    /**
     * Background Async Task to Create new product
     * */
    class CreateNewProduct extends AsyncTask<String, String, String> {

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
	    super.onPreExecute();
	    pDialog = new ProgressDialog(CreatePostActivity.this);
	    pDialog.setMessage("Creating Post... May take a while if images are large");
	    pDialog.setIndeterminate(false);
	    pDialog.setCancelable(true);
	    pDialog.show();
	}

	/**
	 * Creating product
	 * */
	protected String doInBackground(String... args) {
	    String name = LocalPreferences.GetString(CreatePostActivity.this,
		    LocalPreferences.USERNAME);
	    String title = inputTitle.getText().toString();
	    String body = inputBody.getText().toString();
	    String quantity = inputQuantity.getText().toString();

	    // Form validation (Client-side)
	    if (title != null && "".equals(title.trim())) {
		success = 0;
		message = getResources().getString(R.string.empty_title);
		return null;
	    }

	    if (quantity != null && "".equals(quantity.trim())) {
		success = 0;
		message = getResources()
			.getString(R.string.invalid_quantity_no);
		return null;
	    }

	    int quantity_no = Integer.parseInt(quantity);

	    if (quantity_no <= 0) {
		success = 0;
		message = getResources()
			.getString(R.string.invalid_quantity_no);
		return null;
	    }


	    String sightingDate;
	    String sightingTime;
	    
	    if (lastModified > 0) {
			sightingDate = DateFormat.getDateInstance().format(lastModified);
			sightingTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(lastModified);
		}
	    else {
	    	sightingDate = "invalid";
			sightingTime = "invalid";
	    }
		String submissionTime = DateFormat.getTimeInstance(DateFormat.SHORT).format(System.currentTimeMillis());
		String submissionDate = DateFormat.getDateInstance().format(System.currentTimeMillis());
		
	    // Building Parameters
	    
		List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("name", name));
	    params.add(new BasicNameValuePair("title", title));
	    params.add(new BasicNameValuePair("body", body));
	    params.add(new BasicNameValuePair("sighting_time", sightingTime));
	    params.add(new BasicNameValuePair("sighting_date", sightingDate));
	    params.add(new BasicNameValuePair("created_at", submissionTime + "-" + submissionDate));
    
	    
	    // getting JSON Object
	    // Note that create product URL accepts POST method
	    json = jsonParser.makeHttpRequest(url_create_product, "POST",
		    params);
	    int pid;
	    try {
		if (json.getInt(TAG_SUCCESS) == 1) {
		    pid = json.getInt("pid");
		    for (String s : photoPaths) {
				String[] parts = s.split("/");
				String filename = parts[parts.length - 1];
				String path = s.replaceFirst(filename, "");
				NetworkHelper.UploadImage(path, filename, pid);
		    }
		}
	    } catch (Exception e) {
		e.printStackTrace();
	    }

	    // check log cat for response
	    Log.d("Create Response", json.toString());

	    // check for success tag
	    try {
		success = json.getInt(TAG_SUCCESS);
		message = json.getString(TAG_MESSAGE);
	    } catch (JSONException e) {
		e.printStackTrace();
		success = 0;
	    }

	    return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
	    // dismiss the dialog once done
	    pDialog.dismiss();

	    if (success == 1) {
		// successfully created product
		// closing this screen
		finish();
	    } else if (success == 0 || success == -1) {
		// failed to create product
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			CreatePostActivity.this);
		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.setTitle("Submission Error");

		alertDialog.setMessage(message);

		final int BUTTON_POSITIVE = -1;
		alertDialog.setButton(BUTTON_POSITIVE, "OK",
			new DialogInterface.OnClickListener() {

			    @Override
			    public void onClick(DialogInterface dialog,
				    int which) {
				// Do nothing once user clicks OK.
			    }
			});

		// Set the icon to error icon.
		alertDialog.setIcon(R.drawable.ic_error);

		// Show the alert dialog to user
		alertDialog.show();
	    }
	}
    }
}