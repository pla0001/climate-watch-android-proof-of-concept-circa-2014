package org.earthwatch.climatewatch;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBar.Tab;
import android.support.v7.app.ActionBar.TabListener;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

@SuppressWarnings("deprecation")
public class MainActivity extends ActionBarActivity implements TabListener {

    ViewPager viewPager;
    TabsPagerAdapter tabAdapter;
    ActionBar actionBar;

    String[] tabs = { "Profile", "Sighting", "Social" };
    String input_profile_image_path;
    String username;
    Bitmap scaledBitmap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_main);

	tabAdapter = new TabsPagerAdapter(getSupportFragmentManager());
	viewPager = (ViewPager) findViewById(R.id.pager);

	viewPager = (ViewPager) findViewById(R.id.pager);
	actionBar = getSupportActionBar();
	tabAdapter = new TabsPagerAdapter(getSupportFragmentManager());

	viewPager.setAdapter(tabAdapter);
	actionBar.setHomeButtonEnabled(false);
	actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

	// Adding Tabs
	for (String tab_name : tabs) {
	    actionBar.addTab(actionBar.newTab().setText(tab_name)
		    .setTabListener(this));
	}
	viewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

	    @Override
	    public void onPageSelected(int position) {
		actionBar.setSelectedNavigationItem(position);
	    }

	    @Override
	    public void onPageScrolled(int arg0, float arg1, int arg2) {
	    }

	    @Override
	    public void onPageScrollStateChanged(int arg0) {
	    }
	});

	actionBar.setSelectedNavigationItem(1);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.user_pro, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

	switch (item.getItemId()) {
	case R.id.action_create_post:
	    gotoAddPostScreen(null);
	    return true;
	    // case R.id.action_settings:
	    // gotoSettingsScreen(null);
	    // return true;
	case R.id.action_refresh:
	    // TODO: call retry in browse posts listfragment
	    // rather than using this hackish method.
	    MainActivity.this.getSupportActionBar()
		    .setSelectedNavigationItem(0);
	    MainActivity.this.getSupportActionBar()
		    .setSelectedNavigationItem(2);
	    MainActivity.this.getSupportActionBar()
		    .setSelectedNavigationItem(0);
	    MainActivity.this.getSupportActionBar()
		    .setSelectedNavigationItem(2);
	    return true;
	case R.id.action_about:
	    gotoAboutScreen(null);
	    return true;
	case R.id.action_sign_out:
	    signOut(null);
	    return true;
	}

	return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTabReselected(Tab arg0, FragmentTransaction arg1) {
	// TODO Auto-generated method stub

    }

    @Override
    public void onTabSelected(Tab tab, FragmentTransaction arg1) {

	viewPager.setCurrentItem(tab.getPosition());

    }

    @Override
    public void onTabUnselected(Tab arg0, FragmentTransaction arg1) {
	// TODO Auto-generated method stub

    }

    public void gotoSettingsScreen(View view) {
	Intent intent = new Intent(this, SettingsActivity.class);
	startActivity(intent);
    }

    public void gotoAboutScreen(View view) {
	Intent intent = new Intent(this, About.class);
	startActivity(intent);
    }

    public void gotoViewPostScreen(View view) {
	Intent intent = new Intent(this, ViewPostActivity.class);
	startActivity(intent);
    }

    public void gotoAddPostScreen(View view) {
	Intent intent = new Intent(this, CreatePostActivity.class);
	startActivity(intent);
    }

    public void gotoLoginScreen(View view) {
	Intent intent = new Intent(this, LoginActivity.class);
	startActivity(intent);
    }

    public void gotoBrowsePostsScreen(View view) {
	Intent intent = new Intent(this, BrowsePosts.class);
	startActivity(intent);
    }

    int IMAGE_PICKER_SELECT = 2557;

    public static String getPathFromCameraData(Intent data, Context context) {
	Uri selectedImage = data.getData();
	String[] filePathColumn = { MediaStore.Images.Media.DATA };
	Cursor cursor = context.getContentResolver().query(selectedImage,
		filePathColumn, null, null, null);
	cursor.moveToFirst();
	int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	String picturePath = cursor.getString(columnIndex);
	return picturePath;
	// cursor.close();
	// return BitmapFactory.decodeFile(picturePath);
    }

    public void selectPhotoOnClick(View view) {

	Intent i = new Intent(Intent.ACTION_PICK,
		android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	startActivityForResult(i, IMAGE_PICKER_SELECT);

    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	System.out.println("onActivityResult");
	if (requestCode == IMAGE_PICKER_SELECT
		&& resultCode == Activity.RESULT_OK) {
	    input_profile_image_path = getPathFromCameraData(data, this);
	    ImageView profpic = (ImageView) findViewById(R.id.selectProfileImage);
	    TextView txt = (TextView) findViewById(R.id.profileImageLbl);
	    System.out.println("about to call async");
	    new UpdateProfilePictureTask(profpic, txt, input_profile_image_path)
		    .execute();
	}
    }

    @Override
    public void onBackPressed() {
	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
		MainActivity.this);
	AlertDialog alertDialog = alertDialogBuilder.create();

	alertDialog.setTitle("Sign out?");
	alertDialog.setMessage("Are you sure you want to sign out?");

	final int BUTTON_POSITIVE = -1;
	final int BUTTON_NEGATIVE = -2;
	alertDialog.setButton(BUTTON_POSITIVE, "Sign out",
		new DialogInterface.OnClickListener() {

		    @Override
		    public void onClick(DialogInterface dialog, int which) {
			signOut(null);
		    }
		});
	alertDialog.setButton(BUTTON_NEGATIVE, "Cancel",
		new DialogInterface.OnClickListener() {

		    @Override
		    public void onClick(DialogInterface dialog, int which) {
			// do nothing
		    }
		});
	alertDialog.show();
    }

    public void signOut(View view) {
	finish();
    }

    private class UpdateProfilePictureTask extends
	    AsyncTask<String, Void, Bitmap> {

	private ImageView bmImage;
	String path;
	TextView infoText;

	public UpdateProfilePictureTask(ImageView bmImage, TextView infoText,
		String path) {
	    this.bmImage = bmImage;
	    this.path = path;
	    this.infoText = infoText;
	}

	protected void onPreExecute() {

	}

	@SuppressLint("NewApi")
	protected Bitmap doInBackground(String... urls) {
	    System.out.println(path);
	    final Bitmap temp = BitmapFactory.decodeFile(path);
	    System.out.println("decoded");
	    setInfoText("Setting profile\n image..");
	    scaledBitmap = Bitmap.createScaledBitmap(temp, bmImage.getWidth(),
		    bmImage.getHeight(), false);

	    runOnUiThread(new Runnable() {

		@Override
		public void run() {
		    bmImage.setColorFilter(null);
		    bmImage.setImageBitmap(scaledBitmap);
		    bmImage.setBackgroundDrawable(getApplicationContext()
			    .getResources().getDrawable(
				    R.drawable.profileborder));
		}
	    });
	    scaledBitmap = Bitmap.createScaledBitmap(temp, bmImage.getWidth(),
		    bmImage.getHeight(), false);

	    System.out.println("set as bitmap");
	    username = LocalPreferences.GetString(getApplicationContext(),
		    LocalPreferences.USERNAME);
	    FileOutputStream FOS = null;
	    try {
		FOS = openFileOutput(username + ".png", Context.MODE_PRIVATE);
	    } catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	    }
	    System.out.println("written to SD");

	    scaledBitmap.compress(Bitmap.CompressFormat.PNG, 90, FOS);
	    try {
		FOS.flush();
		FOS.close();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    String output_profile_image_path = getApplicationContext()
		    .getFileStreamPath(username + ".png").getAbsolutePath();

	    if (output_profile_image_path != null
		    || output_profile_image_path != "") {
		String[] parts = output_profile_image_path.split("/");
		String filename = parts[parts.length - 1];
		String path = output_profile_image_path.replaceFirst(filename,
			"");

		System.out.println("uploading");
		setInfoText("Uploading profile\n image..");
		NetworkHelper.UploadProfileImage(path, filename, username);
		setInfoText("Profile image set\nsuccessfully.");
	    }

	    LocalPreferences.PutValue(getApplication(),
		    LocalPreferences.PROFILE_IMAGE_URL, username);
	    return null;
	}

	protected void onPostExecute(Bitmap result) {

	}
    }

    private void setInfoText(final String val) {
	runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
		((TextView) findViewById(R.id.profileImageLbl)).setText(val);
	    }
	});
    }
}
