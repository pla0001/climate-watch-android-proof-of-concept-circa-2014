package org.earthwatch.climatewatch;

import java.io.InputStream;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout.LayoutParams;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ViewPostActivity extends ActionBarActivity {

    String pid;

    TextView nameText;
    TextView titleText;
    TextView bodyText;
    // Progress Dialog
    private ProgressDialog pDialog;

    // Creating JSON Parser object
   JSONParser jsonParser = new JSONParser();

   ArrayList<CommentItem> commentsList;
   // url to get all products list
   private static String url_all_comments = "http://crazychimps.com/climatewatch/get_comments.php";

   // comments JSONArray
   JSONArray comments = null;

   JSONObject json;

    // single product url
    private static final String url_post_details = "http://crazychimps.com/climatewatch/get_post_details.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_POST = "post";
    private static final String TAG_PID = "pid";
    private static final String TAG_NAME = "name";
    private static final String TAG_TITLE = "title";
    private static final String TAG_BODY = "body";
    private static final String TAG_UPVOTES = "upvotes";
    private static final String TAG_DOWNVOTES = "downvotes";
    private static final String TAG_PHOTOS = "images";
    private static final String TAG_COMMENTS = "comments";
    private static final String TAG_COMMENT_BODY = "comment";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_PROFILE_IMAGE = "profile_image_url";
    private static final String TAG_SIGHTING_TIME = "sighting_time";
    private static final String TAG_SIGHTING_DATE = "sighting_date";
    private static final String TAG_SUBMISSION_TIME_DATE = "created_at";
    private static final String TAG_COMMENT_TIME = "comment_time";
    private static final String TAG_COMMENT_DATE = "comment_date";
    
    private boolean alreadyUpvoted = false;
    private boolean alreadyDownvoted = false;
    @Override
    public void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.view_post);

	// getting product details from intent
	Intent i = getIntent();
	commentsList = new ArrayList<CommentItem>();

	// getting product id (pid) from intent
	pid = i.getStringExtra(TAG_PID);

	System.out.println("PID: " + pid);

	nameText = (TextView) findViewById(R.id.nameText);
	titleText = (TextView) findViewById(R.id.titleText);
	bodyText = (TextView) findViewById(R.id.bodyText);

	// enable back button
	getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	// Getting complete product details in background thread
	new GetProductDetails().execute();
	new LoadAllComments().execute();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == android.R.id.home) {
	    finish();
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    public void SetTextViewStrings(final String name, final String title,
	    final String body) {
	runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
			nameText.setText(name);
			titleText.setText(title);
			bodyText.setText(body);
	    }
	});
    }

    public Bitmap getBitmapFromUrl(String urlString) {
	try {
	    int pos = urlString.lastIndexOf('/') + 1;
	    URL url = new URL(urlString.substring(0, pos)
		    + URLEncoder.encode(urlString.substring(pos), "utf-8"));

	    // URL url = new URL(urlString);
	    // try this url =
	    // "http://0.tqn.com/d/webclipart/1/0/5/l/4/floral-icon-5.jpg"
	    HttpGet httpRequest = null;

	    httpRequest = new HttpGet(url.toURI());

	    HttpClient httpclient = new DefaultHttpClient();
	    HttpResponse response = (HttpResponse) httpclient
		    .execute(httpRequest);

	    HttpEntity entity = response.getEntity();
	    BufferedHttpEntity b_entity = new BufferedHttpEntity(entity);
	    InputStream input = b_entity.getContent();

	    Bitmap bitmap = BitmapFactory.decodeStream(input);
	    return bitmap;

	} catch (Exception ex) {
	    ex.printStackTrace();
	    return null;
	}
    }

    public void SetVotesText(final int up, final int down) {
	runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
		TextView upvoteText = (TextView) findViewById(R.id.upvotesText);
		upvoteText.setText(Integer.toString(up));
		TextView downvoteText = (TextView) findViewById(R.id.downvotesText);
		downvoteText.setText(Integer.toString(down));
	    }
	});
    }

    public void ChangeUpvoteText(final int i) {
	runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		TextView upvoteText = (TextView) findViewById(R.id.upvotesText);
		int curVal = Integer.decode((String) upvoteText.getText());
		int newVal = curVal + i;
		upvoteText.setText(Integer.toString(newVal));
	    }
	});
    }

    public void ChangeDownvoteText(final int i) {
	runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		TextView downvoteText = (TextView) findViewById(R.id.downvotesText);
		int curVal = Integer.decode((String) downvoteText.getText());
		int newVal = curVal + i;
		downvoteText.setText(Integer.toString(newVal));
	    }
	});
    }

    public void OnClickUpvote(final View view) {
	runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		NetworkHelper nH = new NetworkHelper();
		ImageButton upVote = (ImageButton) view;
		if (upVote.getBackground().getConstantState() == getResources()
			.getDrawable(R.drawable.ic_vote_up).getConstantState()) {
			
			// Click upvote
			
			upVote.setBackgroundResource(R.drawable.ic_vote_up_pressed);
		    
		    if (alreadyUpvoted == false && alreadyDownvoted == false) {
		    
		    	// First vote - upvote
		    	
		    	nH.Vote(Integer.decode(pid), 1, 0);
		    	ChangeUpvoteText(1);
		    
			    alreadyUpvoted = true;
		    	alreadyDownvoted = false;
		    	
		    }
		    else if (alreadyDownvoted == true) {
		    	
		    	// Previously downvoted
		    	
		    	// Remove downvote - Add upvote
		    	
		    	ImageButton downVote = (ImageButton) findViewById(R.id.dislikeButton);
		    	downVote.setBackgroundResource(R.drawable.ic_vote_down);
		    	upVote.setBackgroundResource(R.drawable.ic_vote_up_pressed);
		    	nH.Vote(Integer.decode(pid), 1, -1);
		    	ChangeUpvoteText(1);
		    	ChangeDownvoteText(-1);
		    	
		    	alreadyUpvoted = true;
		    	alreadyDownvoted = false;
		    	
		    }
		    
		} else {
			
			// Unclick upvote
			
			System.out.println("got here");
			
			upVote.setBackgroundResource(R.drawable.ic_vote_up);
		    nH.Vote(Integer.decode(pid), -1, 0);
		    ChangeUpvoteText(-1);
		    
		    alreadyUpvoted = false;
		}
	    }
	});
    }

    public void OnClickDownvote(final View view) {
	runOnUiThread(new Runnable() {
	    @Override
	    public void run() {
		NetworkHelper nH = new NetworkHelper();
		ImageButton downVote = (ImageButton) view;
		
		if (downVote.getBackground().getConstantState() == getResources().getDrawable(R.drawable.ic_vote_down).getConstantState()) {
			
			
			downVote.setBackgroundResource(R.drawable.ic_vote_down_pressed);
		    
			if (alreadyUpvoted == false && alreadyDownvoted == false) {
			    
		    	// First downvote - add downvote
				
				nH.Vote(Integer.decode(pid), 0, 1);
			    ChangeDownvoteText(1);
			    
			    alreadyDownvoted = true;
			    alreadyUpvoted = false;
				
			}
			else if (alreadyUpvoted == true) {
		    	// Previously upvoted
		    	
		    	// Remove upvote - Add downvote
				
				ImageButton upVote = (ImageButton) findViewById(R.id.likeButton);
				upVote.setBackgroundResource(R.drawable.ic_vote_up);
		    	downVote.setBackgroundResource(R.drawable.ic_vote_down_pressed);
		    	nH.Vote(Integer.decode(pid), -1, 1);
		    	ChangeUpvoteText(-1);
		    	ChangeDownvoteText(1);
		    	
		    	alreadyDownvoted = true;
		    	alreadyUpvoted = false;
				
			}
			
			
		} else {
			
			// Unclick upvote
			
			downVote.setBackgroundResource(R.drawable.ic_vote_down);
		    nH.Vote(Integer.decode(pid), 0, -1);
		    ChangeDownvoteText(-1);
		    
		    alreadyDownvoted = false;
		}
	    }
	});
    }
    
    public void PostCommentClick(View view){
    	final EditText commentInput = (EditText) findViewById(R.id.inputComment);
    	long millis = System.currentTimeMillis();
    	String text = commentInput.getText().toString();
    	String time = DateFormat.getTimeInstance(DateFormat.SHORT).format(millis);
    	String date = DateFormat.getDateInstance().format(millis);
    	
    	if(text.replaceAll(" ", "") != "" && text != null){
	    	Comment(pid, LocalPreferences.GetString(this,
	    			LocalPreferences.USERNAME),
	    			text, time, date);
	    	try{
	    		runOnUiThread(new Runnable() {
					@Override
					public void run() {
				    	commentInput.setText("");
						((TextView)findViewById(R.id.commentSubmittedText)).setText("Submitted");
					}
				});
	    	}catch(Exception e){}
	    	
    	}
    }

    public void SetImages(final ArrayList<String> imageUrls) {
	runOnUiThread(new Runnable() {

	    @Override
	    public void run() {
		LinearLayout photoScroll = (LinearLayout) findViewById(R.id.photoScrollLayout_ViewPost);
		photoScroll.removeAllViews();
		for (String url_string : imageUrls) {
		    ImageView picB = new ImageView(getApplicationContext());
		    // new DownloadImageToScrollTask(picB, getWindowManager(),
		    // photoScroll).execute(url);
		    try {

			new DownloadImageTask(picB).execute(url_string);

			// resize bitmap:
			DisplayMetrics displaymetrics = new DisplayMetrics();
			getWindowManager().getDefaultDisplay().getMetrics(
				displaymetrics);
			int height = displaymetrics.heightPixels;
			int width = displaymetrics.widthPixels;

			final int maxSize = (int) (width * 0.85);

			picB.setScaleType(ScaleType.FIT_CENTER);
			LayoutParams lp = new LayoutParams(
				(int) (maxSize * 1.1), (int) (maxSize * 1.1));

			Bitmap placeholder = BitmapFactory.decodeResource(
				getResources(), R.drawable.image_loading);
			
			picB.setImageBitmap(placeholder);

			photoScroll.addView(picB, lp);
		    } catch (Exception e1) {
			e1.printStackTrace();
		    }

		}
		if (imageUrls.size() == 0) {
		    photoScroll.setLayoutParams(new LayoutParams(0, 0));
		}
	    }
	});
    }

    /**
     * Background Async Task to Get complete product details
     * */
    class GetProductDetails extends AsyncTask<String, String, String> {

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
	    super.onPreExecute();
	    pDialog = new ProgressDialog(ViewPostActivity.this);
	    pDialog.setMessage("Getting post details..");
	    pDialog.setIndeterminate(false);
	    pDialog.setCancelable(true);
	    pDialog.show();
	}

	/**
	 * Creating post view
	 * */
	protected String doInBackground(String... args) {
		

	    // Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("pid", pid));

	    // getting JSON Object
	    // Note that create product url accepts POST method
	    JSONObject json = jsonParser.makeHttpRequest(url_post_details,
		    "GET", params);

	    // check log cat fro response
	    Log.d("Create Response", json.toString());

	    // check for success tag
	    try {
		int success = json.getInt(TAG_SUCCESS);

		if (success == 1) {
		    JSONArray postObj = json.getJSONArray(TAG_POST); // JSON
		    // Array

		    // get first post object from JSON Array
		    JSONObject post = postObj.getJSONObject(0);

		    // *** Get the image urls associated with this post:
		    ArrayList<String> urlArray = new ArrayList<String>();
		    JSONArray jsonImageArray = json.getJSONArray(TAG_PHOTOS);
		    for (int i=0; i<jsonImageArray.length(); i++) {
				try {
				    String jsonObject = jsonImageArray.getString(i);
				    urlArray.add(jsonObject);
				} catch (JSONException e) {
				    e.printStackTrace();
				}
		    }
			SetImages(urlArray);

		    // post with this pid found
		    // display post data in textViews
		    String submission_time_date = post.getString(TAG_SUBMISSION_TIME_DATE);
		    
		    String image_taken_time = post.getString(TAG_SIGHTING_TIME);
		    String image_taken_date = post.getString(TAG_SIGHTING_DATE);
		    
		    String[] submission = submission_time_date.split("-");
		    
		    String submission_time = submission[0];
		    String submission_date = submission[1];
		    
		    if (image_taken_date.contains("invalid") || image_taken_time.contains("invalid")) {
		    	SetTextViewStrings(
				    	"Post submitted at " + submission_time + " on the " + submission_date + " by " + post.getString(TAG_NAME),
					    post.getString(TAG_TITLE), post.getString(TAG_BODY));
		    }
		    else {
			    SetTextViewStrings(
				    	"Image taken at " + image_taken_time + " on the " + image_taken_date + "." + "\n" + 
					    "\nPost submitted at " + submission_time + " on the " + submission_date + " by " + post.getString(TAG_NAME),
					    post.getString(TAG_TITLE), post.getString(TAG_BODY));
		    }


		    SetVotesText(post.getInt(TAG_UPVOTES),
			    post.getInt(TAG_DOWNVOTES));
		} else {
		    // failed to create product
		}
	    } catch (JSONException e) {
		e.printStackTrace();
	    }

	    return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
	    // dismiss the dialog once done
	    pDialog.dismiss();
	}
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
	
		private ImageView bmImage;
	
		public DownloadImageTask(ImageView bmImage) {
		    this.bmImage = bmImage;
		}
	
		protected void onPreExecute() {
	
		    // mDialog =
		    // ProgressDialog.show(getApplicationContext(),"Please wait...",
		    // "Retrieving data ...", true);
		}
	
		protected Bitmap doInBackground(String... urls) {
		    String urldisplay = urls[0];
		    urldisplay = urldisplay.replaceAll(" ", "%20");
		    Bitmap bitmap = null;
		    try {
			InputStream in = new java.net.URL(urldisplay).openStream();
			bitmap = BitmapFactory.decodeStream(in);
		    } catch (Exception e) {
			Log.e("Error", "image download error");
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		    }
	
		    // resize bitmap:
		    DisplayMetrics displaymetrics = new DisplayMetrics();
		    getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		    int height = displaymetrics.heightPixels;
		    int width = displaymetrics.widthPixels;
	
		    final int maxSize = (int) (width * 0.85);
	
		    int outWidth;
		    int outHeight;
		    int inWidth = bitmap.getWidth();
		    int inHeight = bitmap.getHeight();
		    if (inWidth > inHeight) {
			outWidth = maxSize;
			outHeight = (inHeight * maxSize) / inWidth;
		    } else {
			outHeight = maxSize;
			outWidth = (inWidth * maxSize) / inHeight;
		    }
		    Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth,
			    outHeight, false);
		    return resizedBitmap;
		}
	
		protected void onPostExecute(Bitmap result) {
		    // set image of your imageview
		    bmImage.setImageBitmap(result);
		    // close
		    // mDialog.dismiss();
		}
    }
    
    
    class LoadAllComments extends AsyncTask<String, String, String> {
    	int success;

    	/**
    	 * Before starting background thread Show Progress Dialog
    	 * */
    	@Override
    	protected void onPreExecute() {
    	    super.onPreExecute();
    	    Log.d("AsyncTask", "Starting LoadAllComments AsyncTask...");
    	}

    	/**
    	 * getting All products from url
    	 * */
    	protected String doInBackground(String... args) {
    	    // Building Parameters
    	    List<NameValuePair> params = new ArrayList<NameValuePair>();
    	    params.add(new BasicNameValuePair("pid", pid));
    	    // getting JSON string from URL
    	    json = jsonParser.makeHttpRequest(url_all_comments, "GET", params);

    	    // Check your log cat for JSON reponse
    	    Log.d("All Products: ", json.toString());

    	    try {
	    		// Checking for SUCCESS TAG
	    		success = json.getInt(TAG_SUCCESS);
	
	    		if (success == 1) {
	    		    // products found
	    		    // Getting Array of Products
	    		    comments = json.getJSONArray(TAG_COMMENTS);
	    		    commentsList = new ArrayList<CommentItem>();
	
	    		    // looping through All Products
	    		    for (int i = 0; i < comments.length(); i++) {
		    			JSONObject c = comments.getJSONObject(i);
		
		    			// Storing each json item in variable
		    			String name = c.getString(TAG_NAME);
		    			String comment = c.getString(TAG_COMMENT_BODY);
		    			String imageUrl = c.getString(TAG_PROFILE_IMAGE);
		    			String time = c.getString(TAG_COMMENT_TIME);
		    			String date = c.getString(TAG_COMMENT_DATE);
		    			
		    			
		    			CommentItem temp = new CommentItem();
		    			temp.setName(name);
		    			temp.setComment(comment);
		    			temp.setImageUrl("http://crazychimps.com/climatewatch/" + imageUrl);
		    			temp.setSubmissionTime(time);
		    			temp.setSubmissionDate(date);
		    			
		    			// creating new HashMap
		    			/*HashMap<String, String> map = new HashMap<String, String>();
		
		    			// adding each child node to HashMap key => value
		    			map.put(TAG_NAME, name);
		    			map.put(TAG_COMMENT_BODY, comment);*/
		
		    			// adding HashList to ArrayList
		    			commentsList.add(temp);
	    		    }
	    		} else {
	    		    /*
	    		     * // no products found // Launch Add New product Activity
	    		     * Intent i = new Intent(getApplicationContext(),
	    		     * CreatePostActivity.class); // Closing all previous
	    		     * activities i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
	    		     * startActivity(i);
	    		     */
	    		}
    	    } catch (JSONException e) {
    	    	e.printStackTrace();
    	    }

    	    return null;
    	}

    	/**
    	 * After completing background task Dismiss the progress dialog
    	 * **/
    	protected void onPostExecute(String file_url) {
    		try{
//    		    loadingPosts.setVisibility(LinearLayout.GONE);
    	
    		    // If an error has occurred while loading posts, show an error
    		    // message
    		    if (success == 0) {
	    			/*AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
	    				ViewPostActivity.this);
	    			AlertDialog alertDialog = alertDialogBuilder.create();
	    	
	    			alertDialog.setTitle("Loading Error");
	    			try {
	    			    alertDialog.setMessage(json.getString(TAG_MESSAGE));
	    			} catch (JSONException e) {
	    			    e.printStackTrace();
	    			    return;
	    			}
    	
	    			final int BUTTON_POSITIVE = -1;
	    			alertDialog.setButton(BUTTON_POSITIVE, "OK",
	    				new DialogInterface.OnClickListener() {
	    	
	    				    @Override
	    				    public void onClick(DialogInterface dialog,
	    					    int which) {
	    					// Do nothing
	    				    }
	    				});
	    	
	    			// Set the icon to Climate Watch.
	    			alertDialog.setIcon(R.drawable.ic_error);
	    	
	    			// Show the alert dialog to user
	    			alertDialog.show();*/
    		    } else if (success == 1) {
	    			
    		    }
    	
    		    // updating UI from Background Thread
    		    try {
	    			runOnUiThread(new Runnable() {
	    			    public void run() {
		    				/**
		    				 * Updating parsed JSON data into Layout
		    				 * */
		    				/*ListAdapter adapter = new SimpleAdapter(ViewPostActivity.this,
		    					commentsList, R.layout.comment_item, new String[] {
		    						TAG_NAME, TAG_COMMENT_BODY }, new int[] {
		    						R.id.commentNameText, R.id.commentText });*/
	    			    	
	    			    	CommentAdapter adapter = new CommentAdapter(ViewPostActivity.this, commentsList);
		    				
		    			    LinearLayout layout = (LinearLayout)findViewById(R.id.commentsList);
		    			    layout.removeAllViews();
				    		final int adapterCount = adapter.getCount();
				    		for (int i = adapterCount-1; i>=0; i--) {
				    			View item = adapter.getView(i, null, null);
				    			layout.addView(item);
				    		}
	    			    }
	    			});
    		    } catch (NullPointerException e) {
	    			Log.e("Error",
	    				"Cannot get activity to update browse posts fragment.");
    		    }
    		}catch(Exception e){}

    	}
    }
    

	
	public void Comment(String pid, String name, String comment, String time, String date){
		new Comment().execute(pid, name, comment, time, date);
	}
	
	class Comment extends AsyncTask<String, String, String> {

		/**
		 * Before starting background thread Show Progress Dialog
		 * */
		@Override
		protected void onPreExecute() {
		    super.onPreExecute();
		}

		/**
		 * Creating product
		 * */
		protected String doInBackground(String... args) {

			String url_comment = "http://crazychimps.com/climatewatch/comment.php";
			String pid = args[0];
			String name = args[1];
			String comment = args[2];
			String time = args[3];
			String date = args[4];
			
			System.out.println(args);
		    // Building Parameters
		    List<NameValuePair> params = new ArrayList<NameValuePair>();
		    params.add(new BasicNameValuePair("pid", pid));
		    params.add(new BasicNameValuePair("name", name));
		    params.add(new BasicNameValuePair("comment", comment));
		    params.add(new BasicNameValuePair("comment_time", time));
		    params.add(new BasicNameValuePair("comment_date", date));
		    
		    // getting JSON Object
		    // Note that create product URL accepts POST method
			JSONParser jsonParser = new JSONParser();
		    JSONObject json = jsonParser.makeHttpRequest(url_comment, "POST",
			    params);
		    
		    return null;
		}

		/**
		 * After completing background task Dismiss the progress dialog
		 * **/
		protected void onPostExecute(String result) {
		    super.onPostExecute(result);
		    new LoadAllComments().execute();
		}
	}
}
