package org.earthwatch.climatewatch;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class CommunityTestArea extends Activity {
    public void onCreate(Bundle saved) {
	super.onCreate(saved);
	setContentView(R.layout.test_area);
    }

    public void PostDataTest(View view) {
	Intent i = new Intent(this, CreatePostActivity.class);
	startActivity(i);
    }

    public void GetDataTest(View view) {
	Intent i = new Intent(this, BrowsePosts.class);
	startActivity(i);
    }
}
