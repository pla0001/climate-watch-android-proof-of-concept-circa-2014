package org.earthwatch.climatewatch;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class Sighting extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
	    @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
	// TODO Auto-generated method stub

	View rootView = inflater.inflate(R.layout.fragment_sighting, container,
		false);

	return rootView;

    }
    

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
	super.onActivityCreated(savedInstanceState);

	Button viewLatestSightings = (Button) getActivity().findViewById(
		R.id.viewLatestSightings);
	viewLatestSightings.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {
		((ActionBarActivity) getActivity()).getSupportActionBar()
			.setSelectedNavigationItem(2);
	    }

	});
    }
}
