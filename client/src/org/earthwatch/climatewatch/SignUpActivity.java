package org.earthwatch.climatewatch;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SignUpActivity extends ActionBarActivity {

    private Bitmap scaledBitmap;

    private ProgressDialog pDialog;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";

    private String input_profile_image_path;
    private String output_profile_image_path;
    private String username;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_sign_up);
	getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	EditText emailEdit = (EditText) findViewById(R.id.editTextEmail);
	TextView emailTextView = (TextView) findViewById(R.id.email_policy_tv);
	// Comment/Remove lines below when we need email
	emailEdit.setVisibility(View.GONE);
	emailTextView.setVisibility(View.GONE);

	// Prevent the window from opening by default
	this.getWindow().setSoftInputMode(
		WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

	Button signupButton = (Button) findViewById(R.id.signupBtn);
	signupButton.setOnClickListener(new OnClickListener() {

	    @Override
	    public void onClick(View v) {

		new CreateNewAccount().execute();
	    }
	});
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.sign_up, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_about) {
	    Intent intent = new Intent(this, About.class);
	    startActivity(intent);
	    return true;
	} else if (id == android.R.id.home) {
	    finish();
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    int IMAGE_PICKER_SELECT = 2557;

    public void onAddPhotoClick(View view) {
	Intent i = new Intent(Intent.ACTION_PICK,
		android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
	startActivityForResult(i, IMAGE_PICKER_SELECT);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	if (requestCode == IMAGE_PICKER_SELECT
		&& resultCode == Activity.RESULT_OK) {
	    input_profile_image_path = getPathFromCameraData(data, this);
	    runOnUiThread(new Runnable() {
		@Override
		public void run() {
		    ImageView profpic = (ImageView) findViewById(R.id.selectProfileImageBtn);
		    ImageButton btn = (ImageButton) findViewById(R.id.selectProfileImageBtn);
		    TextView txt = (TextView) findViewById(R.id.selectProfileImageLbl);
		    Bitmap temp = BitmapFactory
			    .decodeFile(input_profile_image_path);
		    int padding = (int) getResources().getDimension(
			    R.dimen.dashedPadding);

		    profpic.setColorFilter(null);
		    scaledBitmap = Bitmap.createScaledBitmap(temp,
			    btn.getWidth() + 2 * padding, btn.getWidth() + 2
				    * padding, false);
		    profpic.setImageBitmap(scaledBitmap);
		    btn.setBackgroundDrawable(null);
		    txt.setText("Looks great!");

		}
	    });

	}
    }

    public static String getPathFromCameraData(Intent data, Context context) {
	Uri selectedImage = data.getData();
	String[] filePathColumn = { MediaStore.Images.Media.DATA };
	Cursor cursor = context.getContentResolver().query(selectedImage,
		filePathColumn, null, null, null);
	cursor.moveToFirst();
	int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
	String picturePath = cursor.getString(columnIndex);
	return picturePath;
	// cursor.close();
	// return BitmapFactory.decodeFile(picturePath);
    }

    class CreateNewAccount extends AsyncTask<String, String, String> {
	int success = 0;
	String message = "";

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
	    super.onPreExecute();
	    pDialog = new ProgressDialog(SignUpActivity.this);
	    pDialog.setMessage("Creating account..");
	    pDialog.setIndeterminate(false);
	    pDialog.setCancelable(true);
	    pDialog.show();
	}

	/**
	 * Creating product
	 * */
	protected String doInBackground(String... args) {
	    String url_sign_up = "http://crazychimps.com/climatewatch/new_account.php";

	    EditText nameEdit = (EditText) findViewById(R.id.editTextName);
	    EditText usernameEdit = (EditText) findViewById(R.id.editTextUsername);
	    EditText emailEdit = (EditText) findViewById(R.id.editTextEmail);
	    emailEdit.setVisibility(View.GONE);
	    EditText passwordEdit = (EditText) findViewById(R.id.editTextPassword);
	    String name = nameEdit.getText().toString();
	    String username = usernameEdit.getText().toString();
	    String email = emailEdit.getText().toString();
	    String password = passwordEdit.getText().toString();

	    boolean valid = true;

	    if (name == null || "".equals(name)) {
		success = 0;
		valid = false;
		message += "Please enter your full name.\n";
	    }

	    if (username == null || "".equals(username)) {
		success = 0;
		valid = false;
		message += "Please enter a username.\n";
	    }

	    if (username.contains(" ")) {
		success = 0;
		valid = false;
		message += "Username cannot contain spaces.\n";
	    }

	    // Email validation shouldn't go beyond having an @ sign
	    // Email should also be optional
	    if (email != null && !"".equals(email) && !email.contains("@")) {
		success = 0;
		valid = false;
		message += "Please type in a valid email address.\n";
	    }

	    if (password == null || "".equals(password)) {
		success = 0;
		valid = false;
		message += "Please enter a password for your account.\n";
	    }

	    if (!valid) {
		return null;
	    }

	    LocalPreferences.PutValue(getApplication(),
		    LocalPreferences.USERNAME, username);

	    FileOutputStream FOS = null;
	    try {
		FOS = openFileOutput(username + ".png", Context.MODE_PRIVATE);
	    } catch (FileNotFoundException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	    }

	    if (scaledBitmap != null) {
		scaledBitmap.compress(Bitmap.CompressFormat.PNG, 90, FOS);
	    } else {

		// Set to a default bitmap
		Resources res = getApplicationContext().getResources();
		Bitmap temp = BitmapFactory.decodeResource(res,
			R.drawable.defaultdp);
		ImageButton btn = (ImageButton) findViewById(R.id.selectProfileImageBtn);
		int padding = (int) getResources().getDimension(
			R.dimen.dashedPadding);
		scaledBitmap = Bitmap.createScaledBitmap(temp, btn.getWidth()
			+ 2 * padding, btn.getWidth() + 2 * padding, false);
		scaledBitmap.compress(Bitmap.CompressFormat.PNG, 90, FOS);
	    }
	    System.out.println(scaledBitmap.getHeight());
	    try {
		FOS.flush();
		FOS.close();
	    } catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	    }

	    output_profile_image_path = getApplicationContext()
		    .getFileStreamPath(username + ".png").getAbsolutePath();

	    String registrationDate = DateFormat.getDateInstance().format(
		    System.currentTimeMillis());

	    // Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("name", name));
	    params.add(new BasicNameValuePair("username", username));
	    params.add(new BasicNameValuePair("email", email));
	    params.add(new BasicNameValuePair("password", password));
	    params.add(new BasicNameValuePair("created_at", registrationDate));

	    // getting JSON Object
	    // Note that create product URL accepts POST method
	    JSONParser jsonParser = new JSONParser();
	    JSONObject json = jsonParser.makeHttpRequest(url_sign_up, "POST",
		    params);

	    // check for success tag
	    try {
		success = json.getInt(TAG_SUCCESS);
		message = json.getString(TAG_MESSAGE);
	    } catch (JSONException e) {
		e.printStackTrace();
		success = 0;
	    }

	    if (success == 1) {
		if (output_profile_image_path != null
			|| output_profile_image_path != "") {
		    String[] parts = output_profile_image_path.split("/");
		    String filename = parts[parts.length - 1];
		    String path = output_profile_image_path.replaceFirst(
			    filename, "");
		    NetworkHelper.UploadProfileImage(path, filename, username);
		}
	    }

	    return null;

	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
	    // dismiss the dialog once done
	    pDialog.dismiss();

	    if (success == 1) {
		Toast.makeText(SignUpActivity.this, message, Toast.LENGTH_LONG);
		Intent i = new Intent(SignUpActivity.this, LoginActivity.class);
		startActivity(i);
		finish();
	    } else if (success == -1 || success == 0) {
		// failed to create product
		Log.e("Account", "Failed to create account.");
		Log.e("Message", message);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			SignUpActivity.this);
		AlertDialog alertDialog = alertDialogBuilder.create();

		alertDialog.setTitle("Account Creation Error");
		alertDialog.setMessage(message);

		final int BUTTON_POSITIVE = -1;
		alertDialog.setButton(BUTTON_POSITIVE, "OK",
			new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog,
				    int which) {
			    }
			});

		// Set the icon to Climate Watch.
		alertDialog.setIcon(R.drawable.ic_error);

		// Show the alert dialog to user
		alertDialog.show();
	    }
	}
    }
}
