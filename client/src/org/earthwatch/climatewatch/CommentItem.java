package org.earthwatch.climatewatch;

public class CommentItem {
	String name;
	String comment;
	String imageUrl;
	String time;
	String date;
	
	public CommentItem(){
		
	}
	


	public void setName(String v){
		name = v;
	}
	public void setComment(String v){
		comment = v;
	}
	public void setImageUrl(String url){
		imageUrl = url;
	}
	public String getName(){return name;}
	public String getComment(){return comment;}
	public String getImageUrl(){return imageUrl;}

	public String getSubmissionTime() {
		return time;
	}

	public String getSubmissionDate() {
		return date;
	}
	public void setSubmissionTime(String time) {
		this.time = time;
	}
	
	public void setSubmissionDate(String date) {
		this.date = date;
	}

}
