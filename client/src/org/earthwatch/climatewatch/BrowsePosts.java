package org.earthwatch.climatewatch;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;

public class BrowsePosts extends ListFragment {

    // Creating JSON Parser object
    JSONParser jParser = new JSONParser();

    ArrayList<HashMap<String, String>> postsList;
    // url to get all products list
    private static String url_all_products = "http://crazychimps.com/climatewatch/get_test.php";

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    private static final String TAG_POSTS = "posts";
    private static final String TAG_PID = "pid";
    private static final String TAG_TITLE = "title";
    private static final String TAG_NAME = "name";
    private static final String TAG_MESSAGE = "message";

    LinearLayout loadingPosts;
    LinearLayout noInternetConnection;

    // products JSONArray
    JSONArray posts = null;

    JSONObject json;

    View rootView;

    Button btnRetryConnection;
    int success = 0;

    private static final int CLIENT_FAILURE = -1;
    private static final int SERVER_FAILURE = 0;
    private static final int SUCCESS = 1;

    @Override
    public View onCreateView(LayoutInflater inflater,
	    @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
	rootView = inflater.inflate(R.layout.all_posts, container, false);

	// Hashmap for ListView
	postsList = new ArrayList<HashMap<String, String>>();

	// Loading products in Background Thread
	new LoadAllProducts().execute();

	loadingPosts = (LinearLayout) rootView
		.findViewById(R.id.loading_layout);
	loadingPosts.setVisibility(LinearLayout.VISIBLE);

	noInternetConnection = (LinearLayout) rootView
		.findViewById(R.id.no_connection_layout);

	noInternetConnection.setVisibility(LinearLayout.GONE);

	btnRetryConnection = (Button) rootView
		.findViewById(R.id.btnRetryButton);

	btnRetryConnection.setOnClickListener(new View.OnClickListener() {

	    @Override
	    public void onClick(View v) {
		retry();
	    }
	});

	return rootView;
    }

    public void retry() {
	noInternetConnection.setVisibility(LinearLayout.GONE);
	loadingPosts.setVisibility(LinearLayout.VISIBLE);
	getListView().setVisibility(ListView.GONE);
	new LoadAllProducts().execute();
    }

    @Override
    public void onListItemClick(ListView l, View view, int position, long id) {
	// getting values from selected ListItem
	String pid = ((TextView) view.findViewById(R.id.pid)).getText()
		.toString();

	// Starting new intent
	Intent in = new Intent(getActivity(), ViewPostActivity.class);
	// sending pid to next activity
	in.putExtra(TAG_PID, pid);

	// starting new activity and expecting some response back
	startActivityForResult(in, 100);
    }

    // Response from Edit Product Activity
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	super.onActivityResult(requestCode, resultCode, data);
	// if result code 100
	if (resultCode == 100) {
	    // if result code 100 is received
	    // means user edited/deleted product
	    // reload this screen again
	    Intent intent = getActivity().getIntent();
	    getActivity().finish();
	    startActivity(intent);
	}

    }

    /**
     * Background Async Task to Load all product by making HTTP Request
     * */
    class LoadAllProducts extends AsyncTask<String, String, String> {

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
	    super.onPreExecute();
	    Log.d("AsyncTask", "Starting LoadAllProducts AsyncTask...");
	}

	/**
	 * getting All products from url
	 * */
	protected String doInBackground(String... args) {
	    // Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    // getting JSON string from URL
	    json = jParser.makeHttpRequest(url_all_products, "GET", params);

	    // Check your log cat for JSON reponse
	    Log.d("All Products: ", json.toString());

	    try {
		// Checking for SUCCESS TAG
		success = json.getInt(TAG_SUCCESS);

		if (success == SUCCESS) {
		    // products found
		    // Getting Array of Products
		    posts = json.getJSONArray(TAG_POSTS);

		    // looping through All Products
		    for (int i = 0; i < posts.length(); i++) {
			JSONObject c = posts.getJSONObject(i);

			// Storing each json item in variable
			String id = c.getString(TAG_PID);
			String title = c.getString(TAG_TITLE);
			String name = "By " + c.getString(TAG_NAME);

			// creating new HashMap
			HashMap<String, String> map = new HashMap<String, String>();

			// adding each child node to HashMap key => value
			map.put(TAG_PID, id);
			map.put(TAG_TITLE, title);
			map.put(TAG_NAME, name);

			// adding HashList to ArrayList
			postsList.add(map);
		    }
		} else {
		    /*
		     * // no products found // Launch Add New product Activity
		     * Intent i = new Intent(getApplicationContext(),
		     * CreatePostActivity.class); // Closing all previous
		     * activities i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		     * startActivity(i);
		     */
		}
	    } catch (JSONException e) {
		e.printStackTrace();
	    }

	    return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
	    try {
		loadingPosts.setVisibility(LinearLayout.GONE);

		// If an error has occurred while loading posts, show an error
		// message
		if (success == CLIENT_FAILURE) {
		    noInternetConnection.setVisibility(LinearLayout.VISIBLE);
		} else if (success == SERVER_FAILURE) {
		    AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			    getActivity());
		    AlertDialog alertDialog = alertDialogBuilder.create();

		    alertDialog.setTitle("Loading Error");
		    try {
			alertDialog.setMessage(json.getString(TAG_MESSAGE));
		    } catch (JSONException e) {
			e.printStackTrace();
			return;
		    }

		    final int BUTTON_POSITIVE = -1;
		    alertDialog.setButton(BUTTON_POSITIVE, "OK",
			    new DialogInterface.OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog,
					int which) {
				    // Do nothing
				}
			    });

		    // Set the icon to Climate Watch.
		    alertDialog.setIcon(R.drawable.ic_error);

		    // Show the alert dialog to user
		    alertDialog.show();
		} else if (success == SUCCESS) {
		    try {
			getListView().setVisibility(ListView.VISIBLE);
		    } catch (IllegalStateException e) {
			// User has left this fragment, and consequently has
			// been
			// deleted
			// However the async task is not, so just print an error
			// message.
			Log.e("Error",
				"Error while trying to set list view to visible.");
		    }
		}

		// updating UI from Background Thread
		try {
		    getActivity().runOnUiThread(new Runnable() {
			public void run() {
			    /**
			     * Updating parsed JSON data into ListView
			     * */
			    ListAdapter adapter = new SimpleAdapter(
				    getActivity(), postsList,
				    R.layout.post_item, new String[] { TAG_PID,
					    TAG_TITLE, TAG_NAME }, new int[] {
					    R.id.pid, R.id.title, R.id.name });
			    // updating listview
			    setListAdapter(adapter);
			}
		    });
		} catch (NullPointerException e) {
		    Log.e("Error",
			    "Cannot get activity to update browse posts fragment.");
		}
	    } catch (Exception e) {
	    }

	}
    }
}