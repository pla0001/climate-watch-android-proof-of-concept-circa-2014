package org.earthwatch.climatewatch;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;

public class LoginActivity extends ActionBarActivity {
    private ProgressDialog pDialog;

    private static final String TAG_SUCCESS = "success";
    private static final String TAG_MESSAGE = "message";
    private static final String TAG_NAME = "name";
    private static final String TAG_USERNAME = "username";
    private static final String TAG_PASSWORD = "password";
    private static final String TAG_EMAIL = "email";
    private static final String TAG_CREATED_AT = "created_at";
    private static final String TAG_PROFILE_IMAGE_URL = "profile_image_url";
    private static final String TAG_REGISTRATION_DATE = "rego";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.activity_login);
	getSupportActionBar().setDisplayHomeAsUpEnabled(true);

	EditText usernameEdit = (EditText) findViewById(R.id.loginUsername);
	EditText passwordEdit = (EditText) findViewById(R.id.loginPassword);
	usernameEdit.setText(LocalPreferences.GetString(LoginActivity.this,
		LocalPreferences.USERNAME));
	passwordEdit.setText(LocalPreferences.GetString(LoginActivity.this,
		LocalPreferences.PASSWORD));
	((Button) findViewById(R.id.loginBtn))
		.setOnClickListener(new OnClickListener() {
		    @Override
		    public void onClick(View v) {
			new LoginTask().execute();
		    }
		});
	((CheckBox) findViewById(R.id.rememberMeButton))
		.setChecked(LocalPreferences.GetBoolean(this,
			LocalPreferences.REMEMBER_ME));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.login, menu);
	return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
	// Handle action bar item clicks here. The action bar will
	// automatically handle clicks on the Home/Up button, so long
	// as you specify a parent activity in AndroidManifest.xml.
	int id = item.getItemId();
	if (id == R.id.action_about) {
	    Intent intent = new Intent(this, About.class);
	    startActivity(intent);
	    return true;
	} else if (id == android.R.id.home) {
	    finish();
	    overridePendingTransition(0, 0);
	    return true;
	}
	return super.onOptionsItemSelected(item);
    }

    public void gotoUserProfileScreen(View view) {
	Intent intent = new Intent(this, MainActivity.class);
	startActivity(intent);
    }

    class LoginTask extends AsyncTask<String, String, String> {
	int success = 0;
	String message;
	String name;
	String username;
	String password;
	String email;
	String created_at;
	String profile_image_url;
	String regdate;

	/**
	 * Before starting background thread Show Progress Dialog
	 * */
	@Override
	protected void onPreExecute() {
	    super.onPreExecute();
	    

	    
	    pDialog = new ProgressDialog(LoginActivity.this);
	    pDialog.setMessage("Logging in..");
	    pDialog.setIndeterminate(false);
	    pDialog.setCancelable(true);
	    pDialog.show();
	}

	/**
	 * Creating product
	 * */
	protected String doInBackground(String... args) {
	    String url_login = "http://crazychimps.com/climatewatch/login.php";

	    EditText usernameEdit = (EditText) findViewById(R.id.loginUsername);
	    EditText passwordEdit = (EditText) findViewById(R.id.loginPassword);
	    username = usernameEdit.getText().toString();
	    password = passwordEdit.getText().toString();

	    // Building Parameters
	    List<NameValuePair> params = new ArrayList<NameValuePair>();
	    params.add(new BasicNameValuePair("username", username));
	    params.add(new BasicNameValuePair("password", password));

	    // getting JSON Object
	    // Note that create product URL accepts POST method
	    JSONParser jsonParser = new JSONParser();
	    JSONObject json = jsonParser.makeHttpRequest(url_login, "POST",
		    params);

	    System.out.println(json.toString());
	    
	    // check for success tag
	    try {
		success = json.getInt(TAG_SUCCESS);
		message = json.getString(TAG_MESSAGE);
		name = json.getString(TAG_NAME);
		username = json.getString(TAG_USERNAME);
		password = json.getString(TAG_PASSWORD);
		email = json.getString(TAG_EMAIL);
		created_at = json.getString(TAG_CREATED_AT); 
	//	regdate = json.getString(TAG_REGISTRATION_DATE);
		profile_image_url = "http://www.crazychimps.com/climatewatch/"
			+ json.getString(TAG_PROFILE_IMAGE_URL);
	    } catch (JSONException e) {
		e.printStackTrace();
		success = 0;
	    }

	    return null;
	}

	/**
	 * After completing background task Dismiss the progress dialog
	 * **/
	protected void onPostExecute(String file_url) {
	    // dismiss the dialog once done
	    pDialog.dismiss();
	    if (success == 1) {
		if (((CheckBox) findViewById(R.id.rememberMeButton))
			.isChecked()) {
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.USERNAME, username);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.PASSWORD, password);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.NAME, name);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.EMAIL, email);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.CREATED_AT, created_at);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.PROFILE_IMAGE_URL,
			    profile_image_url);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.REMEMBER_ME, true);
		    
		} else {
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.USERNAME, username);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.PASSWORD, "");
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.NAME, name);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.EMAIL, email);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.CREATED_AT, created_at);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.PROFILE_IMAGE_URL,
			    profile_image_url);
		    LocalPreferences.PutValue(LoginActivity.this,
			    LocalPreferences.REMEMBER_ME, false);
		}
		Intent i = new Intent(LoginActivity.this, MainActivity.class);
		startActivity(i);
		finish();
	    } else if (success == 0) {
		// failed to create product
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
			LoginActivity.this);
		AlertDialog alertDialog = alertDialogBuilder.create();
		
		alertDialog.setTitle("Login Error");
		alertDialog.setMessage(message);

		final int BUTTON_POSITIVE = -1;
		alertDialog.setButton(BUTTON_POSITIVE, "OK",
			new DialogInterface.OnClickListener() {
			    @Override
			    public void onClick(DialogInterface dialog,
				    int which) {
			    }
			});

		// Set the icon to error
		alertDialog.setIcon(R.drawable.ic_error);

		// Show the alert dialog to user
		alertDialog.show();
	    }
	   
	}
    }

    public void onBackPressed() {
	super.onBackPressed();
	overridePendingTransition(0, 0);
    }
}