package org.earthwatch.climatewatch;

import java.io.InputStream;
import java.util.ArrayList;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class CommentAdapter extends BaseAdapter {

	private ArrayList listData;

	private LayoutInflater layoutInflater;

	public CommentAdapter(Context context, ArrayList listData) {
		this.listData = listData;
		layoutInflater = LayoutInflater.from(context);
	}

	@Override
	public int getCount() {
		return listData.size();
	}

	@Override
	public Object getItem(int position) {
		return listData.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ViewHolder holder;
		if (convertView == null) {
			
			Boolean even = true;
			// Check if position is even or odd by inspection of first bit http://stackoverflow.com/questions/7342237/check-whether-number-is-even-or-odd
			if ( (position & 1) == 0 ) 
			{ even = true; } // Even
			else 
			{ even = false; } // Odd 
			
			
			convertView = layoutInflater.inflate(R.layout.comment_item, null);
			holder = new ViewHolder();
			holder.nameView = (TextView) convertView.findViewById(R.id.commentNameText);
			holder.commentView = (TextView) convertView.findViewById(R.id.commentText);
			holder.imageView = (ImageView) convertView.findViewById(R.id.commentProfileImage);
			holder.time_date = (TextView) convertView.findViewById(R.id.time_date);
			holder.layout = (LinearLayout) convertView.findViewById(R.id.commentLayout);
			
			if (even) holder.layout.setBackgroundDrawable(convertView.getResources().getDrawable(R.drawable.comment_even));
			else 	  holder.layout.setBackgroundDrawable(convertView.getResources().getDrawable(R.drawable.comment_odd));
			
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		CommentItem comment = (CommentItem) listData.get(position);

		holder.nameView.setText(comment.getName());
		holder.commentView.setText(comment.getComment());
		holder.time_date.setText("Posted " + comment.getSubmissionTime() + " - " + comment.getSubmissionDate());
		if (holder.imageView != null) {
			new DownloadImageTask(holder.imageView).execute(comment.getImageUrl());
		}

		return convertView;
	}

	static class ViewHolder {
		
		LinearLayout layout;
		TextView nameView;
		TextView commentView;
		ImageView imageView;
		TextView time_date;
		
		
	}
	
	private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
		
		private ImageView bmImage;
	
		public DownloadImageTask(ImageView bmImage) {
		    this.bmImage = bmImage;
		}
	
		protected void onPreExecute() {
	
		    // mDialog =
		    // ProgressDialog.show(getApplicationContext(),"Please wait...",
		    // "Retrieving data ...", true);
		}
	
		protected Bitmap doInBackground(String... urls) {
		    String urldisplay = urls[0];
		    urldisplay = urldisplay.replaceAll(" ", "%20");
		    Bitmap bitmap = null;
		    try {
			InputStream in = new java.net.URL(urldisplay).openStream();
			bitmap = BitmapFactory.decodeStream(in);
		    } catch (Exception e) {
			Log.e("Error", "image download error");
			Log.e("Error", e.getMessage());
			e.printStackTrace();
		    }
		    
		    return bitmap;
		}
	
		protected void onPostExecute(Bitmap result) {
		    // set image of your imageview
		    bmImage.setImageBitmap(result);
		    // close
		    // mDialog.dismiss();
		}
    }
}