package org.earthwatch.climatewatch;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView.FindListener;
import android.widget.ImageView;
import android.widget.TextView;

public class UserProfile extends Fragment {

	private FileInputStream FIS;
    private ViewPager viewPager;
    private TabsPagerAdapter adapter;
    private ActionBar actionBar;
    private Bitmap dp;
    private String[] tabs = { "Profile", "Sighting", "Social" };

    @Override
    public View onCreateView(LayoutInflater inflater,
		    @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
	
		View rootView = inflater.inflate(R.layout.user_profile, container,
			false);
		
		return rootView;

    }
    
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
    	super.onActivityCreated(savedInstanceState);
		TextView nameText = (TextView)getActivity().findViewById(R.id.full_name);
		TextView usernameText = (TextView)getActivity().findViewById(R.id.user_name);
		TextView joinDateText = (TextView)getActivity().findViewById(R.id.joined_date);
		TextView profileText = (TextView)getActivity().findViewById(R.id.profileImageLbl);
		ImageView profileImage = (ImageView) getActivity().findViewById(R.id.selectProfileImage);
		
		Context context = getActivity().getApplicationContext();
		String username = LocalPreferences.GetString(getActivity().getApplicationContext(), LocalPreferences.USERNAME);
		try {
			
			
			// Set the dp to the one the user it when they signed up
			FIS = context.openFileInput(username + ".png");
			dp = BitmapFactory.decodeStream(FIS);
			if (dp != null && dp.getWidth() < 300)  {
				
				// Image exists - set it
				profileImage.setColorFilter(null);
				profileImage.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.profileborder));
				profileImage.setImageBitmap(dp);
				
				
				FIS.close();
				
			}
			
			else {
				
				// Image not found - try downloading from server
				
				String profileImageUrl = LocalPreferences.GetString(getActivity(), LocalPreferences.PROFILE_IMAGE_URL);
			
				if(!profileImageUrl.equals("http://www.crazychimps.com/climatewatch/") || !profileImageUrl.equals("")) {

					Log.d("UserProfile","Display image (dp.png) not found may never existed, been deleted or first time login");
					
					new DownloadImageTask(profileImage, username).execute(profileImageUrl);
					
					FIS = context.openFileInput(username + ".png");
					dp = BitmapFactory.decodeStream(FIS);
					if (dp != null) {
						// Image exists - set it
						profileImage.setColorFilter(null);
						profileImage.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.profileborder));
						profileImage.setImageBitmap(dp);	
						profileText.setText("Tap to set a \n new Profile Picture");
						FIS.close();
						
					}
					
				}else{
					
					// Nothing found in the server - set the button to request the user to add an image
					
		 		}
			}
			
			
			
			
		} catch (FileNotFoundException e) {
			
		} catch (IOException e) {
			
			// Couldn't close FOS.
			
			e.printStackTrace();
		}

		nameText.setText(LocalPreferences.GetString(getActivity(), LocalPreferences.NAME));
		usernameText.setText(LocalPreferences.GetString(getActivity(), LocalPreferences.USERNAME));
		joinDateText.setText("Member since " + LocalPreferences.GetString(getActivity(), LocalPreferences.CREATED_AT));
		
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
	// Inflate the menu; this adds items to the action bar if it is present.
	inflater.inflate(R.menu.user_pro, menu);
    }
    
    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {

    	private ProgressDialog mDialog;
    	private ImageView bmImage;
    	String userName;
    	public DownloadImageTask(ImageView bmImage, String userName) {
    	    this.bmImage = bmImage;
    	    this.userName = userName;

    	    
    	    
    	    
    	}

    	protected void onPreExecute() {

    	    // mDialog =
    	    // ProgressDialog.show(getApplicationContext(),"Please wait...",
    	    // "Retrieving data ...", true);
    	}

    	protected Bitmap doInBackground(String... urls) {
    	    String urldisplay = urls[0];
    	    urldisplay = urldisplay.replaceAll(" ", "%20");
    	    Bitmap bitmap = null;
    	    try {
    		InputStream in = new java.net.URL(urldisplay).openStream();
    		bitmap = BitmapFactory.decodeStream(in);
    	    } catch (Exception e) {
    		Log.e("Error", "image download error");
    		Log.e("Error", e.getMessage());
    		e.printStackTrace();
    	    }

    	    // resize bitmap:
    	    /*DisplayMetrics displaymetrics = new DisplayMetrics();
    	    getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
    	    int height = displaymetrics.heightPixels;
    	    int width = displaymetrics.widthPixels;

    	    final int maxSize = (int) (width * 0.85);

    	    int outWidth;
    	    int outHeight;
    	    int inWidth = bitmap.getWidth();
    	    int inHeight = bitmap.getHeight();
    	    if (inWidth > inHeight) {
    		outWidth = maxSize;
    		outHeight = (inHeight * maxSize) / inWidth;
    	    } else {
    		outHeight = maxSize;
    		outWidth = (inWidth * maxSize) / inHeight;
    	    }
    	    Bitmap resizedBitmap = Bitmap.createScaledBitmap(bitmap, outWidth,
    		    outHeight, false);*/
    	    return bitmap;
    	}

    	protected void onPostExecute(Bitmap result) {
    	    // set image of your imageview
    	    bmImage.setImageBitmap(result);
    	    
    	    Context context = getActivity().getApplicationContext();
    	    FileOutputStream FOS = null;
    	    try {
				FOS = context.openFileOutput(userName + ".png" ,Context.MODE_PRIVATE);
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	   
    	   result.compress(Bitmap.CompressFormat.PNG, 90, FOS);
    	    
    	    // close
    	    // mDialog.dismiss();
    	}
    }
}
