package org.earthwatch.climatewatch;

import android.content.Context;
import android.content.SharedPreferences;


public class LocalPreferences {
	public final static String USERNAME = "username";
	public final static String PASSWORD = "password";
	public final static String NAME = "name";
	public final static String PROFILE_IMAGE_URL = "profile_picture_url";
	public final static String EMAIL = "email";
	public final static String CREATED_AT = "created_at";
	public final static String REMEMBER_ME = "remember_me";
	public final static String REG_DATE = "rego";
	
	private final static String PREFERENCES_NAME = "org.earthwatch.climatewatch";

	public static void PutValue(Context ctx, String id, String val){
		GetPrefs(ctx).edit().putString(id, val).commit();
	}
	public static void PutValue(Context ctx, String id, int val){
		GetPrefs(ctx).edit().putInt(id, val).commit();
	}
	public static void PutValue(Context ctx, String id, float val){
		GetPrefs(ctx).edit().putFloat(id, val).commit();
	}
	public static void PutValue(Context ctx, String id, boolean val){
		GetPrefs(ctx).edit().putBoolean(id, val).commit();	
	}
	
	public static String GetString(Context ctx, String id){
		return GetPrefs(ctx).getString(id, "");
	}
	public static int GetInt(Context ctx, String id){
		return GetPrefs(ctx).getInt(id, 0);
	}
	public static float GetFloat(Context ctx, String id){
		return GetPrefs(ctx).getFloat(id, 0);
	}
	public static boolean GetBoolean(Context ctx, String id){
		return GetPrefs(ctx).getBoolean(id, false);
	}
	
	private static SharedPreferences GetPrefs(Context ctx){
		SharedPreferences prefs = 
				ctx.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE);
		return prefs;
	}
}
